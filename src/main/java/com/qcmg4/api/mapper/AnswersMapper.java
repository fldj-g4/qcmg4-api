package com.qcmg4.api.mapper;

import com.qcmg4.api.dto.AnswersDto;
import com.qcmg4.api.models.Answers;
import com.qcmg4.api.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnswersMapper {

    @Autowired
    private QuestionRepository questionRepository;

    public Answers toEntity(AnswersDto dto) {
        var answers = new Answers();
        answers.setId(dto.getId());
        answers.setAnswer(dto.getAnswer());
        var question = questionRepository.findById(dto.getIdQuestion());
        if(question.isPresent()) {
            answers.setQuestion(question.get());
        }else {
            answers.setQuestion(null);
        }
        return answers;
    }

    public AnswersDto toDto(Answers answers) {
        var dto = new AnswersDto();
        dto.setId(answers.getId());
        dto.setAnswer(answers.getAnswer());
        dto.setIdQuestion(answers.getQuestion().getId());
        return dto;
    }
}
