package com.qcmg4.api.mapper;

import com.qcmg4.api.dto.UserDto;
import com.qcmg4.api.models.User;
import com.qcmg4.api.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    @Autowired
    private RoleService roleService;

    public User toEntity(UserDto dto) {
        var user = new User();
        user.setId(dto.getId());
        user.setLogin(dto.getLogin());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setCompany(dto.getCompany());
        user.setRole(dto.getRole());
        return user;
    }

    public UserDto toDto(User user) {
        var dto = new UserDto();
        dto.setId(user.getId());
        dto.setLogin(user.getLogin());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setCompany(user.getCompany());
        dto.setRole(user.getRole());
        return dto;
    }
}
