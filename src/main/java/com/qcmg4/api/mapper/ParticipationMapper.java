package com.qcmg4.api.mapper;

import com.qcmg4.api.dto.AnswersDto;
import com.qcmg4.api.dto.ParticipationDto;
import com.qcmg4.api.models.Answers;
import com.qcmg4.api.models.Participation;
import com.qcmg4.api.repository.AnswerRepository;
import com.qcmg4.api.repository.McqRepository;
import com.qcmg4.api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@Slf4j
public class ParticipationMapper {

    @Autowired
    private UserRepository repository;
    @Autowired
    private McqRepository mcqRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private AnswersMapper mapper;

    public Participation toEntity(ParticipationDto dto) {
        log.info("coverting the dto to entity");
        var participation = new Participation();
        participation.setId(dto.getId());
        participation.setDuration(dto.getDuration());
        participation.setDate(dto.getDate());
        var user = repository.findById(dto.getIdUser());
        if(user.isPresent())
            participation.setUser(user.get());
        else
            participation.setUser(null);
        var answers = new ArrayList<Answers>();
        var mcq = mcqRepository.findById(dto.getIdMcq());
        if(mcq.isPresent())
            participation.setMcq(mcq.get());
        else
            participation.setMcq(null);
        for (AnswersDto item: dto.getAnswers()) {
            answers.add(answerRepository.findById(item.getId()).get());
        }
        participation.setAnswers(answers);
        return participation;
    }

    public ParticipationDto toDto(Participation participation) {
        log.info("converting the entity to dto");
        var dto = new ParticipationDto();
        dto.setId(participation.getId());
        dto.setDuration(participation.getDuration());
        dto.setDate(participation.getDate());
        dto.setIdUser(participation.getUser().getId());
        dto.setIdMcq(participation.getMcq().getId());
        var answers = new ArrayList<AnswersDto>();
        for (Answers item: participation.getAnswers()) {
            answers.add(mapper.toDto(item));
        }
        dto.setAnswers(answers);
        return  dto;
    }
}
