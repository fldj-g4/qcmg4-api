package com.qcmg4.api.mapper;

import java.util.HashSet;

import com.qcmg4.api.dto.McqDto;
import com.qcmg4.api.dto.QuestionDto;
import com.qcmg4.api.models.Mcq;
import com.qcmg4.api.models.Question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class McqMapper {

    @Autowired
    private QuestionMapper questionMapper;

    public Mcq toEntity(McqDto mcqDto){

        Mcq mcqEntity = new Mcq();
        mcqEntity.setName(mcqDto.getName());
        mcqEntity.setDescription(mcqDto.getDescription());

        mcqEntity.setCreatedBy(mcqDto.getCreatedBy());
        mcqEntity.setLastEditionBy(mcqDto.getLastEditionBy());

        mcqEntity.setCreatedAt(mcqDto.getCreatedAt());
        mcqEntity.setLastEditionAt(mcqDto.getLastEditionAt());

        
        if(mcqDto.getQuestions() != null){
            mcqEntity.setQuestions(new HashSet<Question>());
            for (QuestionDto questionDto : mcqDto.getQuestions()) {
                mcqEntity.getQuestions().add( questionMapper.toEntity(questionDto));
            }
        }

        return mcqEntity;
    }

    public McqDto toDto(Mcq mcqEntity){

        McqDto mcqDto = new McqDto();
        mcqDto.setId(mcqEntity.getId());
        mcqDto.setName(mcqEntity.getName());
        mcqDto.setDescription(mcqEntity.getDescription());

        mcqDto.setCreatedBy(mcqEntity.getCreatedBy());
        mcqDto.setLastEditionBy(mcqEntity.getLastEditionBy());

        mcqDto.setCreatedAt(mcqEntity.getCreatedAt());
        mcqDto.setLastEditionAt(mcqEntity.getLastEditionAt());

        if( mcqEntity.getQuestions() != null )
        {
            mcqDto.setQuestions( new HashSet<QuestionDto>());
            for (Question questionEntity : mcqEntity.getQuestions()) {
                mcqDto.getQuestions().add( questionMapper.toDto(questionEntity));
            }
        }
        

        return mcqDto;
    }
    
}
