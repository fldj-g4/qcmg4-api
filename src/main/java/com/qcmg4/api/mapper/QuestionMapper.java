package com.qcmg4.api.mapper;

import com.qcmg4.api.dto.QuestionDto;
import com.qcmg4.api.models.Question;
import com.qcmg4.api.repository.McqRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionMapper {

    @Autowired
    private AnswersMapper answersMapper;
    @Autowired 
    private McqRepository mcqRepo;

    public Question toEntity(QuestionDto questionDto){
        
        Question questionEntity = new Question();
        if(questionDto.getId() != null ){
            questionEntity.setId(questionDto.getId());
        }

        questionEntity.setQuestion(questionDto.getQuestion());
        questionEntity.setAnswer1(questionDto.getAnswer1());
        questionEntity.setAnswer2(questionDto.getAnswer2());
        questionEntity.setAnswer3(questionDto.getAnswer3());
        questionEntity.setAnswer4(questionDto.getAnswer4());
        questionEntity.setAnswer5(questionDto.getAnswer5());
        questionEntity.setGood_answer(questionDto.getGood_answer());
        
        if(questionDto.getIdMcq() != null){
            questionEntity.setMcq(mcqRepo.findById(questionDto.getIdMcq()).orElseThrow(() -> new RuntimeException("Question not found")) );
        }

        return questionEntity;
        
    }

    public QuestionDto toDto(Question questionEntity){
        
        QuestionDto questionDto = new QuestionDto();
        questionDto.setId(questionEntity.getId());
        
        questionDto.setQuestion(questionEntity.getQuestion());
        
        questionDto.setAnswer1(questionEntity.getAnswer1());
        questionDto.setAnswer2(questionEntity.getAnswer2());
        questionDto.setAnswer3(questionEntity.getAnswer3());
        questionDto.setAnswer4(questionEntity.getAnswer4());
        questionDto.setAnswer5(questionEntity.getAnswer5());
        
        questionDto.setGood_answer(questionEntity.getGood_answer());

        if(questionEntity.getMcq() != null){
            questionDto.setIdMcq(questionEntity.getMcq().getId());
        }

        return questionDto;
    
    }
}
