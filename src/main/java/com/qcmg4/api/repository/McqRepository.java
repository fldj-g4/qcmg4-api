package com.qcmg4.api.repository;

import com.qcmg4.api.models.Mcq;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface McqRepository extends CrudRepository<Mcq, Long>{
    
}
