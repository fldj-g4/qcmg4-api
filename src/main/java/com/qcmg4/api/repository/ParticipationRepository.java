package com.qcmg4.api.repository;

import com.qcmg4.api.models.Participation;
import com.qcmg4.api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Long> {
    public List<Participation> getByUser(User user);
}
