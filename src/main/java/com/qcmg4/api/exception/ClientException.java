package com.qcmg4.api.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Slf4j
public class ClientException extends RuntimeException{

    private Integer code;
    private String reason;
    private HttpStatus httpStatus;

    public ClientException(HttpStatus httpStatus, Integer code, String msg, String reason) {
        super(msg);
        this.code = code;
        this.reason = reason;
        this.httpStatus = httpStatus;
        log.debug("on est là {}", this.httpStatus);
    }
}
