package com.qcmg4.api.exception;

import com.qcmg4.api.models.Error;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ClientResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ClientException.class)
    public final ResponseEntity<Error> handleClientException(ClientException ex) {
        var err = new Error();
        err.setCode(ex.getCode());
        err.setMessage(ex.getMessage());
        err.setReason(ex.getReason());
        err.setStatus(String.valueOf(ex.getHttpStatus().value()));
        return new ResponseEntity<>(err, ex.getHttpStatus());
    }
}
