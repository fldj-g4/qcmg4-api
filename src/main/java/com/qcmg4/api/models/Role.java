package com.qcmg4.api.models;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "role")
public class Role extends AbstractEntity{

    private String name;

    public Role() {
    }

}
