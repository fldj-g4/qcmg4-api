package com.qcmg4.api.models;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "answers")
public class Answers extends AbstractEntity{

    private String answer;

    @ManyToOne
    @JoinColumn(name = "idQuestion")
    private Question question;

}
