package com.qcmg4.api.models;

import com.qcmg4.api.dto.UserDto;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private final transient UserDto user;

    public JwtResponse(String jwttoken, UserDto user) {
        this.jwttoken = jwttoken;
        this.user = user;
    }

    public String getToken() {
        return this.jwttoken;
    }
    public UserDto getUser() {return  this.user;}
}