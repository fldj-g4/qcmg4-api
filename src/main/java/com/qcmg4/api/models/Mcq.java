package com.qcmg4.api.models;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.time.Instant;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mcq")
public class Mcq extends AbstractEntity{
    private String name;
    private String description;
    private String createdBy;
    private String lastEditionBy;
    private Instant createdAt;
    private Instant lastEditionAt;

    @OneToMany(cascade=CascadeType.ALL, mappedBy = "mcq")
    @JsonManagedReference
    private Set<Question> questions;

    
    public boolean addQuestion(Question question){
        return this.questions.add(question);
    }

    public boolean removeQuestion(Question question){
        return this.questions.remove(question);
    }
}
