package com.qcmg4.api.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_tab")
public class User extends AbstractEntity{

    private String login;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    private String password;

    private String company;

    @ManyToOne
    @JoinColumn(name = "idRole")
    private Role role;

}
