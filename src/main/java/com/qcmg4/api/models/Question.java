package com.qcmg4.api.models;

import lombok.Data;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Data
@Entity
@Table(name = "question")
public class Question extends AbstractEntity{
    private String question;

    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String answer5;
    private String good_answer;

    @ManyToOne
    @JoinColumn(name = "idMcq")
    @JsonBackReference
    private Mcq mcq;

}
