package com.qcmg4.api.models;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Data
@Entity
@Table(name = "participation")
public class Participation extends AbstractEntity{

    private Instant date;

    private int duration;

    @ManyToOne
    @JoinColumn(name = "idUser")
    private User user;

    @ManyToMany
    @JoinTable( name = "Answers_Participation_Associations",
            joinColumns = @JoinColumn( name = "idParticipation" ),
            inverseJoinColumns = @JoinColumn( name = "idAnswers" ))
    private List<Answers> answers;


    @ManyToOne
    @JoinColumn(name = "idMcq")
    private Mcq mcq;
}
