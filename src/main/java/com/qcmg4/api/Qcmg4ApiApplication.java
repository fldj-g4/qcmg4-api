package com.qcmg4.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Qcmg4ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Qcmg4ApiApplication.class, args);
	}

}
