package com.qcmg4.api.service;

import java.util.List;

import com.qcmg4.api.dto.QuestionDto;
import com.qcmg4.api.models.Mcq;
import com.qcmg4.api.models.Question;

public interface QuestionService {

    List<Question> getAllQuestions();

    Question createQuestion(Question question);

    Question getQuestionById(Long questionId);

    void deleteQuestion(Long questionId);

    Question updateQuestion(Question question, Long questionId);

    Question createQuestionWithMcq(Mcq mcq, Question question);

    void deleteQuestions(List<QuestionDto> questions);
}
