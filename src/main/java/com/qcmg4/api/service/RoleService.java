package com.qcmg4.api.service;

import java.util.List;
import java.util.Optional;

import com.qcmg4.api.models.Role;

public interface RoleService {
    
    public List<Role> getAllRoles();
    public Role createRole(Role role);
    public Optional<Role> getRoleById(long roleId);
    public void deleteRole(long roleId);
    public Role updateRole(Role role);
    

}
