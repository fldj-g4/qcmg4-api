package com.qcmg4.api.service.impl;

import java.util.List;

import com.qcmg4.api.dto.QuestionDto;
import com.qcmg4.api.dto.UserDto;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Mcq;
import com.qcmg4.api.models.Question;
import com.qcmg4.api.repository.QuestionRepository;
import com.qcmg4.api.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService{

    @Autowired
    private QuestionRepository repository;

    @Override
    public List<Question> getAllQuestions() {
        return (List<Question>) repository.findAll();
    }

    @Override
    public Question createQuestion(Question question) {
        return repository.save(question);
    }

    @Override
    public Question getQuestionById(Long questionId) {
        return repository.findById(questionId).orElseThrow(
            () -> new ClientException(HttpStatus.NOT_FOUND, 404, "Question not found","Error during accessing a Question"  ));
    }

    @Override
    public void deleteQuestion(Long questionId) {
        repository.deleteById(questionId);
    }

    @Override
    public Question updateQuestion(Question newQuestion, Long questionId) {
        return repository.findById(questionId)
            .map(question -> {
                question.setQuestion(newQuestion.getQuestion());
                question.setAnswer1(newQuestion.getAnswer1());
                question.setAnswer2(newQuestion.getAnswer2());
                question.setAnswer3(newQuestion.getAnswer3());
                question.setAnswer4(newQuestion.getAnswer4());
                question.setAnswer5(newQuestion.getAnswer5());
                question.setGood_answer(newQuestion.getGood_answer());
                return repository.save(question);
            }).orElseGet( () -> {
                newQuestion.setId(questionId);
                return repository.save(newQuestion);
            });
        }

    @Override
    public Question createQuestionWithMcq(Mcq mcq, Question question) {
        question.setMcq(mcq);
        return repository.save(question);
    }

    @Override
    public void deleteQuestions(List<QuestionDto> questions) {
        questions.forEach(qst -> this.deleteQuestion(qst.getId()));
    }
}