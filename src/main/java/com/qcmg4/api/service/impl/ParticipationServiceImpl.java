package com.qcmg4.api.service.impl;

import com.qcmg4.api.models.Participation;
import com.qcmg4.api.repository.ParticipationRepository;
import com.qcmg4.api.service.ParticipationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipationServiceImpl implements ParticipationService {

    @Autowired
    private ParticipationRepository repository;

    @Override
    public List<Participation> getAll() {
        return repository.findAll();
    }

    @Override
    public Participation createParticipation(Participation participation) {
        return repository.save(participation);
    }

    @Override
    public Optional<Participation> getParticipationById(long id) {
        return repository.findById(id);
    }

    @Override
    public void deleteParticipation(long id) {
        repository.deleteById(id);
    }

    @Override
    public Participation updateParticipation(Participation participation) {
        return createParticipation(participation);
    }
}
