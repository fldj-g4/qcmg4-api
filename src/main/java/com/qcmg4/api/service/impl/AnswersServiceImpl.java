package com.qcmg4.api.service.impl;

import com.qcmg4.api.models.Answers;
import com.qcmg4.api.repository.AnswerRepository;
import com.qcmg4.api.service.AnswersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswersServiceImpl implements AnswersService {

    @Autowired
    private AnswerRepository answerRepository;

    @Override
    public List<Answers> getAll() {
        return answerRepository.findAll();
    }

    @Override
    public Answers createAnswer(Answers answer) {
        return answerRepository.save(answer);
    }

    @Override
    public Optional<Answers> getAnswerById(long id) {
        return answerRepository.findById(id);
    }

    @Override
    public void deleteAnswer(long answerId) {

         answerRepository.deleteById(answerId);
    }

    @Override
    public Answers updateAnswer(Answers answer) {
        return createAnswer(answer);
    }
}
