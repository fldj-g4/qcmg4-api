package com.qcmg4.api.service.impl;

import java.time.Instant;
import java.util.List;

import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Mcq;
import com.qcmg4.api.models.Question;
import com.qcmg4.api.repository.McqRepository;
import com.qcmg4.api.service.McqService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class McqServiceImpl implements McqService{

    @Autowired 
    private McqRepository repository;

    @Override
    public List<Mcq> getAllMcq() {
        return (List<Mcq>) repository.findAll();
    }

    @Override
    public Mcq createMcq(Mcq mcq, String username) {
        mcq.setCreatedBy(username);
        mcq.setLastEditionBy(username);
        Instant now = Instant.now();
        mcq.setCreatedAt(now);
        mcq.setLastEditionAt(now);
        return repository.save(mcq);
    }

    @Override
    public Mcq getMcqById(Long mcqId) {
        return repository.findById(mcqId).orElseThrow(
            () -> new ClientException(HttpStatus.NOT_FOUND, 404, "Mcq not found","Error during accessing an MCQ"  ));
    }  

    @Override
    public void deleteMcq(Long mcqId) {
        repository.deleteById(mcqId);;
    }

    @Override
    public Mcq updateMcq(Mcq newMcq, long mcqId, String username) {
        return repository.findById(mcqId)
            .map(mcq -> { 
                mcq.setLastEditionBy(username);
                mcq.setLastEditionAt(Instant.now());
                mcq.setName(newMcq.getName());
                mcq.setDescription(newMcq.getDescription());
                return repository.save(mcq);
            }).orElseGet(() -> {
                newMcq.setId(mcqId);
                newMcq.setCreatedBy(username);
                newMcq.setLastEditionBy(username);
                Instant now = Instant.now();
                newMcq.setCreatedAt(now);
                newMcq.setLastEditionAt(now);
                return repository.save(newMcq);
            });
        
    }

    @Override
    public Mcq addQuestionToMcq(Mcq mcq, Question question) {
        //mcq.addQuestion(question);
        return mcq;
    }
    
}
