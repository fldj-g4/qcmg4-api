package com.qcmg4.api.service.impl;

import com.qcmg4.api.dto.UserDto;
import com.qcmg4.api.models.User;
import com.qcmg4.api.repository.UserRepository;
import com.qcmg4.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> getUserById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Optional<User> getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public void deleteUsers(List<UserDto> users) {
        users.forEach(user -> this.deleteUser(user.getId()));
    }

    @Override
    public User updateUser(User user) {
        if(getUserById(user.getId()).isPresent()) return createUser(user);
        return null;
    }
}
