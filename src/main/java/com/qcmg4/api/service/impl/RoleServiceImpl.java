package com.qcmg4.api.service.impl;

import java.util.List;
import java.util.Optional;

import com.qcmg4.api.models.Role;
import com.qcmg4.api.repository.RoleRepository;
import com.qcmg4.api.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService{
    
    @Autowired
    RoleRepository repository;

    @Override
    public List<Role> getAllRoles() {
        
        return (List<Role>) repository.findAll();
    }

    @Override
    public Role createRole(Role role) {
        return repository.save(role);
    }

    @Override
    public Optional<Role> getRoleById(long roleId) {
        return repository.findById(roleId);
    }

    @Override
    public void deleteRole(long roleId) {
        repository.deleteById(roleId);
    }

    @Override
    public Role updateRole(Role role) {
        
       if(repository.findById(role.getId()).isPresent())
           return repository.save(role);
       return null;
    }
}
