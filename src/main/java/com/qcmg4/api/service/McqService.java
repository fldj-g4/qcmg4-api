package com.qcmg4.api.service;

import java.util.List;

import com.qcmg4.api.models.Mcq;
import com.qcmg4.api.models.Question;

public interface McqService {

    List<Mcq> getAllMcq();

    Mcq createMcq(Mcq mcq, String username);

    Mcq getMcqById(Long mcqId);

    void deleteMcq(Long mcqId);

    Mcq updateMcq(Mcq mcq, long mcqId, String username);

    Mcq addQuestionToMcq(Mcq mcq, Question question2);
    
}
