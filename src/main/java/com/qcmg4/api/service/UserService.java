package com.qcmg4.api.service;

import com.qcmg4.api.dto.UserDto;
import com.qcmg4.api.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

     List<User> getAll();
     User createUser(User user);
     Optional<User> getUserById(long userId);
     Optional<User> getUserByLogin(String login);
     void deleteUser(long userId);
     void deleteUsers(List<UserDto> users);
     User updateUser(User user);
}
