package com.qcmg4.api.service;

import com.qcmg4.api.models.Answers;

import java.util.List;
import java.util.Optional;

public interface AnswersService {
    List<Answers> getAll();
    Answers createAnswer(Answers answer);
    Optional<Answers> getAnswerById(long AnswerId);
    void deleteAnswer(long answerId);
    Answers updateAnswer(Answers answer);
}
