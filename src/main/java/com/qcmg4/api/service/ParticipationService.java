package com.qcmg4.api.service;

import com.qcmg4.api.models.Participation;

import java.util.List;
import java.util.Optional;

public interface ParticipationService {
    List<Participation> getAll();
    Participation createParticipation(Participation participation);
    Optional<Participation> getParticipationById(long id);
    void deleteParticipation(long id);
    Participation updateParticipation(Participation participation);
}
