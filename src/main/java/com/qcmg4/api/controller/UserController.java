package com.qcmg4.api.controller;

import com.qcmg4.api.business.UserBusiness;
import com.qcmg4.api.dto.UserDto;
import com.qcmg4.api.mapper.UserMapper;
import com.qcmg4.api.models.Role;
import com.qcmg4.api.models.User;
import com.qcmg4.api.repository.UserRepository;
import com.qcmg4.api.service.RoleService;
import com.qcmg4.api.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.processing.SupportedOptions;
import java.util.ArrayList;
import java.util.List;
@RestController
@Slf4j
@CrossOrigin
@RequestMapping(value= "api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private UserBusiness userBusiness;

    @Autowired
    private UserMapper mapper;

    /**
     * Allows us to recover all the users stored in the database
     * @return
     */
    @ApiOperation("Allows us to recover all the users stored in the database")
    @GetMapping
    public ResponseEntity<List<UserDto>> getAll() {
        log.info("Entering the getAll endpoint");
        var users = userService.getAll();
        List<UserDto> usersDto = new ArrayList<>();
        for (User user: users) {
            usersDto.add(mapper.toDto(user));
        }
        return ResponseEntity.status(HttpStatus.OK).body(usersDto);
    }

    @GetMapping("/me")
    public ResponseEntity<UserDto> currentUser(Authentication authentication) {
        var user = userRepository.findByLogin(authentication.getName());
        if(user.isPresent()) {
            var dto = mapper.toDto(user.get());
            return ResponseEntity.status(HttpStatus.OK).body(dto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    
    /**
     * Allows us to recover the user with the specified Id
     * @param userId : Id of the user we want to get
     * @return
     */
    @ApiOperation("Allows us to recover the user with the specified Id")
    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> getUserById(
            @ApiParam(value = "", required = true) @PathVariable("userId") Long userId
    ) {
        var user = userService.getUserById(userId);

        if(user.isPresent()) {
            var dto = mapper.toDto(user.get());
            return ResponseEntity.status(HttpStatus.OK).body(dto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    /**
     * Allows us to update a user
     * @param dto : object storing the filled user form
     * @return
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @ApiOperation(value = "Allows us to update a user")
    @PutMapping
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto dto) {
        var user = mapper.toEntity(dto);
        userBusiness.loginUpdateValidation(user);
        var newUser = userService.updateUser(user);
        if(newUser != null) return ResponseEntity.status(HttpStatus.OK).body(mapper.toDto(newUser));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    /**
     * Allows us to delete the user with the specified Id
     * @param userId : Id of the user we want to delete
     * @return
     */
    @ApiOperation("Allows us to delete the user with the specified Id")
    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUser(@ApiParam(value = "", required = true) @PathVariable("userId") Long userId) {
        userService.deleteUser(userId);
        if(!userService.getUserById(userId).isPresent())
            return ResponseEntity.status(HttpStatus.OK).body(null);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    /**
     * Allows us to delete multiple users with the specified Id
     * @param users : Ids of the users we want to delete
     * @return
     */
    @ApiOperation("Allows us to delete the users with their specified Id")
    @DeleteMapping("/")
    public ResponseEntity<Void> deleteUsers(@RequestBody List<UserDto> users) {
        userService.deleteUsers(users);
        var notDeletedUsers =
                users.stream().filter(userDto -> userService.getUserById(userDto.getId()).isPresent()).count();
        if(notDeletedUsers == 0)
            return ResponseEntity.status(HttpStatus.OK).body(null);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    /**
     * Allows us to create a new user in the database
     * @param dto : object storing the filled user form
     * @return
     */
    @ApiOperation(value = "Allows us to create a new user in the database")
    @PostMapping
    public ResponseEntity<UserDto> createUser(@RequestBody User dto) {
        userBusiness.userValidation(dto);
        dto.setPassword(bcryptEncoder.encode(dto.getPassword()));
        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.toDto(userService.createUser(dto)));
    }
}
