package com.qcmg4.api.controller;

import com.qcmg4.api.business.AnswersBusiness;
import com.qcmg4.api.dto.AnswersDto;
import com.qcmg4.api.mapper.AnswersMapper;
import com.qcmg4.api.models.Answers;
import com.qcmg4.api.service.AnswersService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/answers")
@Slf4j
public class AnswersController {

    @Autowired
    private AnswersService answersService;

    @Autowired
    private AnswersBusiness answersBusiness;

    @Autowired
    private AnswersMapper mapper;

    //getAll
    /**
     * Allows us to recover all the answers stored in the database
     * @return
     */
    @ApiOperation(value="Allows us to recover all the answers stored in the database")
    @GetMapping
    ResponseEntity<List<AnswersDto>> getAll() {
        var answers = answersService.getAll();
        List<AnswersDto> dto = new ArrayList<>();
        for (Answers answer: answers) {
            dto.add(mapper.toDto(answer));
        }
        return  ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    //getById
    /**
     * Allows us to recover the answer with the specified Id
     * @param id : Id of the Answer we want to get 
     * @return
     */
    @ApiOperation(value="Allows us to recover the answer with the specified Id")
    @GetMapping("/{id}")
    ResponseEntity<AnswersDto> getAnswerById(@ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        var answer = answersService.getAnswerById(id);
        if(answer.isPresent()) {
            var dto = mapper.toDto(answer.get());
            return ResponseEntity.status(HttpStatus.OK).body(dto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    //delete
    /**
     * Allows us to delete the answer with the specified Id
     * @param id : Id of the Answer we want to delete
     * @return
     */
    @ApiOperation(value = "Allows us to delete the answer with the specified Id")
    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteAnswer(@ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        answersService.deleteAnswer(id);
        if(answersService.getAnswerById(id).isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    //create
    /**
     * Allows to create a new Answer
     * @param dto : object storing the filled answer form
     * @return
     */
    @ApiOperation(value = "Allows to create a new Answer")
    @PostMapping
    ResponseEntity<AnswersDto> createAnswer(@RequestBody AnswersDto dto) {
        var answers = mapper.toEntity(dto);
        answersBusiness.answerValidation(answers);
        var answer = answersService.createAnswer(answers);
        var answerDto = mapper.toDto(answer);
        return ResponseEntity.status(HttpStatus.CREATED).body(answerDto);
    }

    //update
    /**
     * Allows us to update an Answer
     * @param dto : object storing the filled answer form
     * @return
     */
    @ApiOperation(value = "Allows us to update an Answer")
    @PutMapping
    ResponseEntity<AnswersDto> updateAnswer(@RequestBody AnswersDto dto) {
        var answers = mapper.toEntity(dto);
        answersBusiness.answerUpdateValidation(answers);
        log.info("la valeur {}", answers.getId());
        var newAnswer = answersService.updateAnswer(answers);
        var answersDto = mapper.toDto(newAnswer);
        return ResponseEntity.status(HttpStatus.OK).body(answersDto);
    }

}
