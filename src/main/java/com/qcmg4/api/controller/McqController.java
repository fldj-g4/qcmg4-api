package com.qcmg4.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.qcmg4.api.business.McqBusiness;
import com.qcmg4.api.business.QuestionBusiness;
import com.qcmg4.api.dto.McqDto;
import com.qcmg4.api.dto.QuestionDto;
import com.qcmg4.api.mapper.McqMapper;
import com.qcmg4.api.mapper.QuestionMapper;
import com.qcmg4.api.models.Mcq;
import com.qcmg4.api.models.Question;
import com.qcmg4.api.service.McqService;
import com.qcmg4.api.service.QuestionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/mcq")
public class McqController {
    
    @Autowired
    private McqService mcqService;
    @Autowired 
    private McqBusiness business;
    @Autowired
    private QuestionBusiness questionBusiness;
    @Autowired 
    private QuestionService questionService;
    @Autowired 
    private McqMapper mcqMapper;
    @Autowired 
    private QuestionMapper questionMapper;



    /**
     * Allows to return a list of all the MCQs that are stored in the database
     * @return
     */
    @ApiOperation( value = "Allows to return a list of all the MCQs that are stored in the database")
    @GetMapping("")
    public ResponseEntity<List<McqDto>> getAllMcq(){

        return ResponseEntity.status(HttpStatus.OK)
                              .body(mcqService.getAllMcq()
                                                .stream()
                                                .map( mcqEntityItem -> mcqMapper.toDto( mcqEntityItem ))
                                                .collect(Collectors.toList()))  ;
    }


    /**
     * Allows to create an MCQ in the database. An example of the request body is 
     * {"name" : "choose a name here", "description" : "describe here"}
     * @param authentication : Parameter that allows usto recover the current logged user
     * @param mcqDto : object storing the filled form
     * @return
     */
    @ApiOperation(value = "Allows to create an MCQ in the database")
    @PostMapping("")
    public ResponseEntity<McqDto> createMcq(Authentication authentication, @RequestBody McqDto mcqDto){
        business.validateMcq(mcqMapper.toEntity( mcqDto) );  
        return ResponseEntity.status(HttpStatus.CREATED)
                             .body( mcqMapper.toDto( 
                                        mcqService.createMcq(
                                            mcqMapper.toEntity(mcqDto), 
                                            authentication.getName()
                                        ) 
                                    ) 
                                );
    }

    /**
     * Allows to return the MCQ with the specified Id
     * @param mcqId : id of the desired MCQ
     * @return 
     */
    @ApiOperation(value = "Allows to return the MCQ with the specified Id")
    @GetMapping("/{mcqId}")
    public ResponseEntity<McqDto> getMcqById(@ApiParam(value = "", required = true) @PathVariable(name="mcqId") Long mcqId){
        return ResponseEntity.status(HttpStatus.OK)
                            .body(mcqMapper.toDto( mcqService.getMcqById(mcqId) )) ;
    }

    /**
     * Allows to delete the MCQ with thhe specified Id
     * @param mcqId : id of the desired MCQ
     */
    @ApiOperation("Allows to delete the MCQ with thhe specified Id")
    @DeleteMapping("/{mcqId}")
    public void deleteMcq(@ApiParam(value = "", required = true) @PathVariable(name="mcqId") Long mcqId){
        mcqService.deleteMcq(mcqId);
    }

    /**
     * Allows to update an MCQ. If the targetted MCQ does not exist (the ID isn't attributed), a new one is created.
     * An example of the request body is {"name" : "choose a name here", "description" : "describe here"}
     * @param authentication : Parameter that allows usto recover the current logged user
     * @param mcqDto : object storing the filled form
     * @param mcqId : id of the MCQ we want to update
     * @return An MCQ 
     */
    @ApiOperation(
        value = "Allows to update an MCQ",
        notes = "Allows to update an MCQ. If the targetted MCQ does not exist (the ID isn't attributed), a new one is created.")
    @PutMapping("/{mcqId}")
    public ResponseEntity<McqDto> updateMcq(Authentication authentication, @RequestBody McqDto mcqDto, @ApiParam(value = "", required = true) @PathVariable(name="mcqId") Long mcqId){
        business.validateMcq(mcqMapper.toEntity(mcqDto));
        return ResponseEntity.status(HttpStatus.OK)
                            .body( 
                                mcqMapper.toDto( 
                                    mcqService.updateMcq(mcqMapper.toEntity(mcqDto), mcqId, authentication.getName()) 
                                )
                            );
    }

    /**
     * Allows us to create a new question and links it directly to an MCQ.
     * Here is an example of the request body
     * {"question":"azerty ?", "answer1":"ans1", "answer2":"ans2", "answer3":"ans3", "answer4":"ans4", "answer5":"ans5", "good_answer":"ans2"}
     * @param mcqId : ID of the MCQ where the question will be added.
     * @param questionDto : object storing the filled question form
     * @return
     */
    @ApiOperation(value = "Allows us to create a new question and links it directly to an MCQ.")
    @PostMapping("/{mcqId}/question")
    public ResponseEntity<McqDto> addQuestionToMcq( @ApiParam(value = "", required = true) @PathVariable(name="mcqId") Long mcqId, @RequestBody QuestionDto questionDto) {
        questionBusiness.validateQuestion(questionMapper.toEntity(questionDto));
        Mcq mcqEntity = mcqService.getMcqById(mcqId);
        Question questionEntity = questionService.createQuestionWithMcq(mcqEntity,  questionMapper.toEntity(questionDto));
        return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body( mcqMapper.toDto( mcqService.addQuestionToMcq(mcqEntity, questionEntity) ) );
    }


}
