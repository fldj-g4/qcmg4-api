package com.qcmg4.api.controller;

import com.qcmg4.api.config.JwtTokenUtil;
import com.qcmg4.api.config.JwtUserDetailsService;
import com.qcmg4.api.mapper.AnswersMapper;
import com.qcmg4.api.mapper.UserMapper;
import com.qcmg4.api.models.JwtRequest;
import com.qcmg4.api.models.JwtResponse;
import com.qcmg4.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*")
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper mapper;

    /**
     * Allows to authenticate in the API.
     * The endpoint needs a request body like this one : {"username": "user", "password":"secret"}
     * If the posted credentials are correct, the response body will contain a token.
     * This token will be used as a cookie for all the other requests to the API (it is a Bearer token).
     * If the posted credentials are bad, an error will be sent.
     * @param authenticationRequest
     * @return
     * @throws Exception
     */
    @ApiOperation(
        value = "Allows to authenticate in the API",
        notes = "The endpoint needs a request body like this one : {\"username\": \"user\", \"password\":\"secret\"}" +
        " If the posted credentials are correct, the response body will contain a token." +
        " This token will be used as a cookie for all the other requests to the API (it is a Bearer token)." +
        " If the posted credentials are bad, an error will be sent."
    )
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        String login = authenticationRequest.getUsername();

        final UserDetails userDetails = userDetailsService.loadUserByUsername(login);

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token, mapper.toDto(userService.getUserByLogin(login).get())));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
