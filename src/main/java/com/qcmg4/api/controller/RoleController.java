package com.qcmg4.api.controller;

import java.util.List;

import com.qcmg4.api.business.RoleBusiness;
import com.qcmg4.api.models.Role;
import com.qcmg4.api.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping("/api/role")
public class RoleController {

    @Autowired
    private RoleService service;

    @Autowired 
    private RoleBusiness roleBusiness;

    /**
     * Allows us to recover all the Questions stored in the database. 
     * @return
     */
    @GetMapping
    @ApiOperation(value = "Allows us to recover all the Questions stored in the database.")
    public ResponseEntity<List<Role>> getAllRoles(){
        var roles = service.getAllRoles();
        return ResponseEntity.status(HttpStatus.OK).body(roles);
    }

    /**
     * Allows to create a new role
     * Here is an example of the request body 
     * {"name" : "admin"}
     * @param role : object storing the filled question form
     * @return
     */
    @ApiOperation("Allows to create a new role")
    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody Role role){
        roleBusiness.validateRole(role);
        var newRole = service.createRole(role);
        return ResponseEntity.status(HttpStatus.CREATED).body(newRole);
    }

    /**
     * Allows us to recover the role with the specified Id
     * @param roleId : Id of the role we want to get
     * @return
     */
    @ApiOperation(value="Allows us to recover the role with the specified Id")
    @GetMapping("/{roleId}")
    public ResponseEntity<Role> getRoleById(@ApiParam(value = "", required = true) @PathVariable(value="roleId") long roleId){
        var role= service.getRoleById(roleId);
        if(role.isPresent())
            return ResponseEntity.status(HttpStatus.OK).body(role.get());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    /**
     * Allows to remove the role with tht specified Id
     * @param roleId : Id of the role we want to remove
     * @return
     */
    @ApiOperation(value = "Allows to remove the role with tht specified Id")
    @DeleteMapping("/{roleId}")
    public ResponseEntity<Void> deleteRole(@ApiParam(value = "", required = true) @PathVariable(value="roleId") long roleId){
        service.deleteRole(roleId);
        if(!service.getRoleById(roleId).isPresent())
            return ResponseEntity.status(HttpStatus.OK).body(null);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    /**
     * Allows us to update the role with the specified ID
     * Here is an example of the request body 
     * {"id" : "90", "name" : "supersuperadmin"}
     * @param role : object storing the filled question form
     * @return
     */
    @ApiOperation("Allows us to update the role with the specified ID")
    @PutMapping
    public ResponseEntity<Role> updateRole(@RequestBody Role role){
        roleBusiness.validateRole(role);
        var newRole = service.updateRole(role);
        return ResponseEntity.status(HttpStatus.OK).body(newRole);
    }


}
