package com.qcmg4.api.controller;

import com.qcmg4.api.business.ParticipationBusiness;
import com.qcmg4.api.dto.ParticipationDto;
import com.qcmg4.api.mapper.ParticipationMapper;
import com.qcmg4.api.models.Participation;
import com.qcmg4.api.repository.ParticipationRepository;
import com.qcmg4.api.service.ParticipationService;

import com.qcmg4.api.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/participation")
@Slf4j
public class ParticipationController {

    @Autowired
    private ParticipationService participationService;

    @Autowired
    private ParticipationMapper mapper;

    @Autowired
    private ParticipationBusiness participationBusiness;

    @Autowired
    private ParticipationRepository participationRepository;

    @Autowired
    private UserService userService;

    //getAll
    /**
     * Allows us to recover all the Participations stored in the database
     * @return
     */
    @ApiOperation(value = "Allows us to recover all the Participations stored in the database")
    @GetMapping
    ResponseEntity<List<ParticipationDto>> getAll() {
        var participation = participationService.getAll();
        List<ParticipationDto> dto = new ArrayList<>();
        for (Participation participation1: participation) {
            dto.add(mapper.toDto(participation1));
        }
        return  ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    //getById
    /**
     * Allows us to recover the Partcipation with the specified Id
     * @param id : Id of the participation we want to get
     * @return
     */
    @ApiOperation(value = "Allows us to recover the Partcipation with the specified Id")
    @GetMapping("/{id}")
    ResponseEntity<ParticipationDto> getParticipationById(@ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        var participation = participationService.getParticipationById(id);
        log.info("participations {}", participation.get());
        if(participation.isPresent()) {
            var dto = mapper.toDto(participation.get());
            return  ResponseEntity.status(HttpStatus.OK).body(dto);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    //delete
    /**
     * Allows to delete the Participation with the specified Id
     * @param id : Id of the paticipation we want to delete
     * @return
     */
    @ApiOperation("Allows to delete the Participation with the specified Id")
    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteParticipation(@ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        participationService.deleteParticipation(id);
        if(participationService.getParticipationById(id).isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    //create
    /**
     * Allows us to create a new participation
     * @param dto : object storing the filled participation form
     * @return
     */
    @ApiOperation("Allows us to create a new participation")
    @PostMapping
    ResponseEntity<ParticipationDto> createParticipation(@RequestBody ParticipationDto dto) {
        var participation = mapper.toEntity(dto);
        participationBusiness.validateParticipation(participation);
        var newParticipation = participationService.createParticipation(participation);
        var participationDto = mapper.toDto(newParticipation);
        log.info("dto {}", participationDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(participationDto);
    }

    //update
    /**
     * Allows us to update a participation
     * @param dto : object storing the filled participation form
     * @return
     */
    @ApiOperation(value = "Allows us to update a participation")
    @PutMapping
    ResponseEntity<ParticipationDto> updateAnswer(@RequestBody ParticipationDto dto) {
        var participation = mapper.toEntity(dto);
        participationBusiness.validateParticipation(participation);
        var newParticipation = participationService.updateParticipation(participation);
        var participationDto = mapper.toDto(newParticipation);
        return ResponseEntity.status(HttpStatus.OK).body(participationDto);
    }

    /**
     * Allows us to update a participation
     * @param dto : object storing the filled participation form
     * @return
     */
    @GetMapping("/me")
    ResponseEntity<List<ParticipationDto>> getAllMyParticipations(Authentication authentication) {
        String login = authentication.getName();
        var user = userService.getUserByLogin(login).get();
        List<Participation> list = participationRepository.getByUser(user);
        return ResponseEntity.status(HttpStatus.OK).body(list.stream().map((s) -> mapper.toDto(s)).collect(Collectors.toList()));
    }
}
