package com.qcmg4.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.qcmg4.api.business.QuestionBusiness;
import com.qcmg4.api.dto.QuestionDto;
import com.qcmg4.api.dto.UserDto;
import com.qcmg4.api.mapper.QuestionMapper;
import com.qcmg4.api.service.QuestionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {
    
    @Autowired
    private QuestionService service;
    @Autowired
    private QuestionBusiness business;
    @Autowired
    private QuestionMapper questionMapper;


    /**
     * Allows us to recover all the Questions stored in the database. 
     * @return
     */
    @ApiOperation(value = "Allows us to recover all the Questions stored in the database")
    @GetMapping("")
    public ResponseEntity<List<QuestionDto>> getAllQuestions(){
        
        return ResponseEntity.status(HttpStatus.OK)
                             .body(service.getAllQuestions()
                                            .stream()
                                            .map( questionEntity ->  questionMapper.toDto(questionEntity))
                                            .collect(Collectors.toList())) ;
    }


    /**
     * Allows us to create a new question and add it in the database.
     * This method does not connect the question to an MCQ.
     * We do not recommend to use it. A better alternative is to use the endpoint mcq/{mcqId}/question
     * Here is an exampke of the request body
     * {"question":"azerty ?", "answer1":"ans1", "answer2":"ans2", "answer3":"ans3", "answer4":"ans4", "answer5":"ans5", "good_answer":"ans2"}
     * @param questionDto : object storing the filled question form
     * @return
     */
    @ApiOperation(
        value = "Allows us to create a new question and add it in the database",
        notes="This method does not connect the question to an MCQ. We do not recommend to use it. A better alternative is to use the endpoint (mcq/{mcqId}/question).")
    @PostMapping("")
    public ResponseEntity<QuestionDto> createQuestion(@RequestBody QuestionDto questionDto){
        business.validateQuestion( questionMapper.toEntity(questionDto) );
        return ResponseEntity.status(HttpStatus.CREATED)
                            .body(questionMapper.toDto(service.createQuestion(questionMapper.toEntity(questionDto))));
    }

    /**
     * Allows us to recover the question that has the specified Id
     * @param questionId : Id of the question we want to get
     * @return
     */
    @ApiOperation(value = "Allows us to recover the question that has the specified Id")
    @GetMapping("/{questionId}")
    public ResponseEntity<QuestionDto> getQuestionById(@ApiParam(value = "", required = true) @PathVariable(name="questionId") Long questionId){
        return ResponseEntity.status(HttpStatus.OK)
                            .body(questionMapper.toDto( service.getQuestionById(questionId) ));
    }

    /**
     * Allows us to delete the question that has the specified Id
     * @param questionId : Id of the question we want to remove
     */
    @ApiOperation(value = "Allows us to delete the question that has the specified Id")
    @DeleteMapping("/{questionId}")
    public void deleteQuestion(@ApiParam(value = "", required = true) @PathVariable(name="questionId") Long questionId){
        service.deleteQuestion(questionId);
    }

    /**
     * Allows us to delete the questions
     */
    @ApiOperation(value = "Allows us to delete the questions that has been specified")
    @DeleteMapping("")
    public void deleteQuestions(@RequestBody List<QuestionDto> questions) {
        service.deleteQuestions(questions);
    }

    /**
     * Allows us to update the question that has the specified Id
     * Here is an example of the request body
     * {"question":"azerty ?", "answer1":"ans1", "answer2":"ans2", "answer3":"ans3", "answer4":"ans4", "answer5":"ans5", "good_answer":"ans2"}
     * @param questionDto : object storing the filled form
     * @param questionId : Id of the question we want to remove
     * @return
     */
    @ApiOperation(value="Allows us to update the question that has the specified Id")
    @PutMapping("/{questionId}")
    public ResponseEntity<QuestionDto> updateQuestion(@RequestBody QuestionDto questionDto, @ApiParam(value = "", required = true) @PathVariable(name="questionId") Long questionId){
        business.validateQuestion(questionMapper.toEntity( questionDto ));
        return ResponseEntity.status(HttpStatus.OK)
                            .body(questionMapper.toDto( service.updateQuestion(questionMapper.toEntity(questionDto), questionId) ));
    }
}
