package com.qcmg4.api.business.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qcmg4.api.business.ErrorTreatment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class ErrorTreatmentImpl implements ErrorTreatment {
    private static ObjectMapper mapper = new ObjectMapper();
    @Override
    public String getErrorMessage(String errorCode) {
        log.debug("Find errorMessage from errorCode : {}", errorCode);
        var errorMessage = "Wrong value for '%s'";

        try {
            var inputStream = ErrorTreatmentImpl.class.getClassLoader()
                    .getResourceAsStream("app_message/qcmg4_api_errors_message.json");
            JsonNode rootNode = mapper.readTree(inputStream);

            var jsonNodeOpt = Optional.ofNullable(rootNode.get(errorCode));
            errorMessage = jsonNodeOpt.map(JsonNode::textValue).orElse(errorMessage);

            log.debug("ErrorMessage found : {}", errorMessage);
        } catch (Exception e) {
            log.debug("Error retrieving message from code {}. A default message was returned.", errorCode);
        }
        return errorMessage;
    }
}
