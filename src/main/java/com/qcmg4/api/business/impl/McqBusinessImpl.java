package com.qcmg4.api.business.impl;

import com.qcmg4.api.business.ErrorTreatment;
import com.qcmg4.api.business.McqBusiness;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Mcq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class McqBusinessImpl implements McqBusiness{
    @Autowired
    private ErrorTreatment errorTreatment;
    
    private static final String ERROR_REASON_INPUT_VALIDATION = "Error during input Question validation";

    @Override
    public void validateMcq(Mcq mcq) {
        if( mcq.getName() instanceof String 
            && !mcq.getName().isBlank()
            && mcq.getName().length() > 2
            && mcq.getName().length() < 200 
            && mcq.getDescription() instanceof String
            && !mcq.getDescription().isBlank()
            && mcq.getDescription().length() > 2
            && mcq.getDescription().length() < 200)
            System.out.println("Mcq validated");
        else{
            System.out.println("Mcq not validated, be careful what you wish for");
            var errorMessage = String.format(errorTreatment.getErrorMessage("402"), "MCQ");
            throw new ClientException(HttpStatus.BAD_REQUEST, 402, errorMessage, ERROR_REASON_INPUT_VALIDATION); 
        }
        
    }
    
}
