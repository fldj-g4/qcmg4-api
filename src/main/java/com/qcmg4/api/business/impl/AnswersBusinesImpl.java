package com.qcmg4.api.business.impl;

import com.qcmg4.api.business.AnswersBusiness;
import com.qcmg4.api.business.ErrorTreatment;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Answers;
import com.qcmg4.api.repository.AnswerRepository;
import com.qcmg4.api.repository.QuestionRepository;
import com.qcmg4.api.service.AnswersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Pattern;

@Component
@Slf4j
public class AnswersBusinesImpl implements AnswersBusiness {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private AnswersService answersService;

    @Autowired
    private ErrorTreatment errorTreatment;

    private static final String ERROR_REASON_INPUT_VALIDATION = "Error during input Answer validation";

    private static final String REGEX_SPACIAL_CHARACTERS = "[_'\"\\-.*\\s(<>=?%&/\\\\]";
    private static final String REGEX_END_CHARACTERS = "[_'\"\\-*\\s(<>=?%&/\\\\]";

    @Override
    public void answerValidation(Answers answers) {

        log.info("validation start");
        if(Pattern.compile("(^" + REGEX_SPACIAL_CHARACTERS + ")|(" + REGEX_END_CHARACTERS + "$)").matcher(answers.getAnswer()).find()) {
            var errorMessage = String.format(errorTreatment.getErrorMessage("403"), "answer");
            log.error(errorMessage);
            throw new ClientException(HttpStatus.BAD_REQUEST, 403, errorMessage, ERROR_REASON_INPUT_VALIDATION);
        }

       questionValidation(answers);
    }

    @Override
    public void answerUpdateValidation(Answers answers) {
            if(!Optional.ofNullable(answerRepository.findById(answers.getId())).get().isPresent()) {
                var errorMessage = String.format(errorTreatment.getErrorMessage("406"), "answer");
                log.error(errorMessage);
                throw new ClientException(HttpStatus.BAD_REQUEST, 406, errorMessage, ERROR_REASON_INPUT_VALIDATION);
            }

            questionValidation(answers);
    }

    void questionValidation(Answers answers) {
        if(!Optional.ofNullable(answers.getQuestion()).isPresent() || !Optional.ofNullable(questionRepository.findById(answers.getQuestion().getId())).get().isPresent()) {
            var errorMessage = String.format(errorTreatment.getErrorMessage("402"), "answer.question");
            log.error(errorMessage);
            throw new ClientException(HttpStatus.BAD_REQUEST, 402, errorMessage, ERROR_REASON_INPUT_VALIDATION);
        }
    }
}
