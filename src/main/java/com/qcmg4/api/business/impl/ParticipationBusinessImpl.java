package com.qcmg4.api.business.impl;

import com.qcmg4.api.business.ErrorTreatment;
import com.qcmg4.api.business.ParticipationBusiness;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Participation;
import com.qcmg4.api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class ParticipationBusinessImpl implements ParticipationBusiness {
    @Autowired
    private UserRepository repository;

    @Autowired
    private ErrorTreatment errorTreatment;

    private static final String ERROR_REASON_INPUT_VALIDATION = "Error during input Answer validation";


    @Override
    public void validateParticipation(Participation participation) {
        log.info("user {}", participation.getUser());
        if(!Optional.ofNullable(participation.getUser()).isPresent() ||!Optional.ofNullable(repository.findById(participation.getUser().getId())).isPresent()) {
            var errorMessage = String.format(errorTreatment.getErrorMessage("402"), "participation.user");
            log.error(errorMessage);
            throw new ClientException(HttpStatus.BAD_REQUEST, 402, errorMessage, ERROR_REASON_INPUT_VALIDATION);
        }
    }
}
