package com.qcmg4.api.business.impl;

import com.qcmg4.api.business.ErrorTreatment;
import com.qcmg4.api.business.QuestionBusiness;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class QuestionBusinessImpl implements QuestionBusiness{

    @Autowired
    private ErrorTreatment errorTreatment;
    
    private static final String ERROR_REASON_INPUT_VALIDATION = "Error during input Question validation";

    @Override
    public void validateQuestion(Question question) {
        if( checkString(question) 
            && checkBlank(question)
            && checkMinLength(question)
            && checkMaxLength(question) )
            System.out.println("Question checked");
        else{
            System.out.println("Question not validated, be careful what you enter in the database");
            var errorMessage = String.format(errorTreatment.getErrorMessage("402"), "question");
            throw new ClientException(HttpStatus.BAD_REQUEST, 402, errorMessage, ERROR_REASON_INPUT_VALIDATION); 
        }

    }
    private boolean checkString(Question question){
        return question.getQuestion() instanceof String 
            && question.getAnswer1() instanceof String
            && question.getAnswer2() instanceof String 
            && question.getAnswer3() instanceof String
            && question.getAnswer4() instanceof String
            && question.getAnswer5() instanceof String
            && question.getGood_answer() instanceof String;
    }

    private boolean checkBlank(Question question){
        return !question.getQuestion().isBlank() 
            && !question.getAnswer1().isBlank()
            && !question.getAnswer2().isBlank()
            && !question.getAnswer3().isBlank()
            && !question.getAnswer4().isBlank()
            && !question.getAnswer5().isBlank()
            && !question.getGood_answer().isBlank();
    }

    private boolean checkMinLength(Question question){
        return question.getQuestion().length() > 2 
            && question.getAnswer1().length() > 2
            && question.getAnswer2().length() > 2
            && question.getAnswer3().length() > 2
            && question.getAnswer4().length() > 2
            && question.getAnswer5().length() > 2
            && question.getGood_answer().length() > 2;
    }

    private boolean checkMaxLength(Question question){
        return question.getQuestion().length() < 200
            && question.getAnswer1().length() < 200
            && question.getAnswer2().length() < 200
            && question.getAnswer3().length() < 200
            && question.getAnswer4().length() < 200
            && question.getAnswer5().length() < 200
            && question.getGood_answer().length() < 200;
    }

}



