package com.qcmg4.api.business.impl;

import com.qcmg4.api.business.ErrorTreatment;
import com.qcmg4.api.business.UserBusiness;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.User;
import com.qcmg4.api.repository.RoleRepository;
import com.qcmg4.api.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Pattern;


@Component
@Slf4j
public class UserBusinessImpl implements UserBusiness {

        @Autowired
        private RoleRepository roleRepository;

        @Autowired
        private UserService userService;

        @Autowired
        private ErrorTreatment errorTreatment;

        private static final String REGEX_SPACIAL_CHARACTERS = "[_'\"\\-.*\\s(<>=?%&/\\\\]";

        private static final String ERROR_REASON_INPUT_VALIDATION = "Error during input User validation";


    @Override
        public void userValidation(User user) {
            roleValidation(user);
            firstNameValidation(user);
            lastNameValidation(user);
            loginValidation(user);
        }

    @Override
    public void loginUpdateValidation(User user) {
        //login
        if(!user.getLogin().equals(userService.getUserById(user.getId()).get().getLogin())) {
            var errorMessage = String.format(errorTreatment.getErrorMessage("402"), "login");
            log.error(errorMessage);
            throw new ClientException(HttpStatus.BAD_REQUEST, 402, errorMessage, ERROR_REASON_INPUT_VALIDATION);
        }
        firstNameValidation(user);
        lastNameValidation(user);
    }

    void roleValidation(User user) {
            //role
            if(Optional.ofNullable(user.getRole()).isPresent() && Optional.ofNullable(roleRepository.findByName(user.getRole().getName())).isEmpty()) {
                var errorMessage = String.format(errorTreatment.getErrorMessage("402"), "role");
                log.error(errorMessage);
                throw new ClientException(HttpStatus.BAD_REQUEST, 402, errorMessage, ERROR_REASON_INPUT_VALIDATION);
            }
        }

        void firstNameValidation(User user) {
            //firstname
            if(Optional.ofNullable(user.getFirstName()).isPresent()) {

                if(user.getFirstName().length() > 300) {
                    var errorMessage = String.format(errorTreatment.getErrorMessage("401"), "firstName");
                    log.error(errorMessage);
                    throw new ClientException(HttpStatus.BAD_REQUEST, 401, errorMessage, ERROR_REASON_INPUT_VALIDATION);
                }

                if(Pattern.compile("(^" + REGEX_SPACIAL_CHARACTERS + ")|(" + REGEX_SPACIAL_CHARACTERS + "$)").matcher(user.getFirstName()).find()) {
                    var errorMessage = String.format(errorTreatment.getErrorMessage("403"), "firstName");
                    log.error(errorMessage);
                    throw new ClientException(HttpStatus.BAD_REQUEST, 403, errorMessage, ERROR_REASON_INPUT_VALIDATION);
                }
            }
        }

        void lastNameValidation(User user) {
            //lastname
            if(Optional.ofNullable(user.getLastName()).isPresent()) {

                if(user.getFirstName().length() > 300) {
                    var errorMessage = String.format(errorTreatment.getErrorMessage("401"), "lastName");
                    log.error(errorMessage);
                    throw new ClientException(HttpStatus.BAD_REQUEST, 401, errorMessage, ERROR_REASON_INPUT_VALIDATION);
                }

                if(Pattern.compile("(^" + REGEX_SPACIAL_CHARACTERS + ")|(" + REGEX_SPACIAL_CHARACTERS + "$)").matcher(user.getLastName()).find()) {
                    var errorMessage = String.format(errorTreatment.getErrorMessage("403"), "lastName");
                    log.error(errorMessage);
                    throw new ClientException(HttpStatus.BAD_REQUEST, 403, errorMessage, ERROR_REASON_INPUT_VALIDATION);
                }
            }
        }

        void loginValidation(User user) {
            //login
            if(Optional.ofNullable(user.getLogin()).isPresent()) {

                if(user.getFirstName().length() > 300) {
                    var errorMessage = String.format(errorTreatment.getErrorMessage("401"), "login");
                    log.error(errorMessage);
                    throw new ClientException(HttpStatus.BAD_REQUEST, 401, errorMessage, ERROR_REASON_INPUT_VALIDATION);
                }
            }
        }


    }