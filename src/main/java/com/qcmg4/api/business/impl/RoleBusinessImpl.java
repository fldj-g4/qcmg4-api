package com.qcmg4.api.business.impl;

import com.qcmg4.api.business.ErrorTreatment;
import com.qcmg4.api.business.RoleBusiness;
import com.qcmg4.api.exception.ClientException;
import com.qcmg4.api.models.Role;

import com.qcmg4.api.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Pattern;

@Component
@Slf4j
public class RoleBusinessImpl implements RoleBusiness{

    @Autowired
    private ErrorTreatment errorTreatment;

    @Autowired
    private RoleRepository roleRepository;

    private static final String REGEX_SPACIAL_CHARACTERS = "[_'\"\\-.*\\s(<>=?%&/\\\\]";

    private static final String ERROR_REASON_INPUT_VALIDATION = "Error during input Role validation";


    @Override
    public void validateRole(Role role) {
        //name
        if(Optional.ofNullable(role.getName()).isPresent()) {

            if(role.getName().length() > 100) {
                var errorMessage = String.format(errorTreatment.getErrorMessage("401"), "role.name");
                log.error(errorMessage);
                throw new ClientException(HttpStatus.BAD_REQUEST, 401, errorMessage, ERROR_REASON_INPUT_VALIDATION);
            }

            if(Pattern.compile("(^" + REGEX_SPACIAL_CHARACTERS + ")|(" + REGEX_SPACIAL_CHARACTERS + "$)").matcher(role.getName()).find()) {
                var errorMessage = String.format(errorTreatment.getErrorMessage("403"), "role.name");
                log.error(errorMessage);
                throw new ClientException(HttpStatus.BAD_REQUEST, 403, errorMessage, ERROR_REASON_INPUT_VALIDATION);
            }

            if(roleRepository.findByName(role.getName()) != null) {
                var errorMessage = String.format(errorTreatment.getErrorMessage("405"), "role");
                log.error(errorMessage);
                throw new ClientException(HttpStatus.BAD_REQUEST, 405, errorMessage, ERROR_REASON_INPUT_VALIDATION);
            }

        }

    }
    
}
