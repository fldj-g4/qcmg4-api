package com.qcmg4.api.business;

import com.qcmg4.api.models.Question;

public interface QuestionBusiness {

    void validateQuestion(Question question);
    
}
