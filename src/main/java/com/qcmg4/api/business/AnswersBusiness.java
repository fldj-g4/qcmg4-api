package com.qcmg4.api.business;

import com.qcmg4.api.models.Answers;

public interface AnswersBusiness {
    void answerValidation(Answers answers);
    void answerUpdateValidation(Answers answers);
}
