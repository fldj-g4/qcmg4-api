package com.qcmg4.api.business;

import com.qcmg4.api.models.Role;

public interface RoleBusiness {
    
    public void validateRole(Role role);
}
