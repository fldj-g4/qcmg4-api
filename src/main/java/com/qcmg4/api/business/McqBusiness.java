package com.qcmg4.api.business;

import com.qcmg4.api.models.Mcq;

public interface McqBusiness {

    void validateMcq(Mcq mcq);
    
}
