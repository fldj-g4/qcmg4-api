package com.qcmg4.api.business;

public interface ErrorTreatment {
    /**
     *
     * @param errorCode
     * @return errorMessage
     */
    String getErrorMessage(String errorCode);
}
