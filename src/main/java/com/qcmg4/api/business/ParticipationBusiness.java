package com.qcmg4.api.business;

import com.qcmg4.api.models.Participation;

public interface ParticipationBusiness {
    public void validateParticipation(Participation participation);
}
