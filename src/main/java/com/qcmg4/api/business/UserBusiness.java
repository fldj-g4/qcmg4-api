package com.qcmg4.api.business;

import com.qcmg4.api.models.User;

public interface UserBusiness {
    void userValidation(User user);
    void loginUpdateValidation(User user);

}
