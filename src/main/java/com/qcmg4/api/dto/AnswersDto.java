package com.qcmg4.api.dto;

import lombok.Data;

@Data
public class AnswersDto {
    private Long id;
    private String answer;
    private Long idQuestion;
}
