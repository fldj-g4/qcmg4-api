package com.qcmg4.api.dto;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class ParticipationDto {
    private Long id;
    private Instant date;
    private int duration;
    private Long idUser;
    private List<AnswersDto> answers;
    private Long idMcq;
}
