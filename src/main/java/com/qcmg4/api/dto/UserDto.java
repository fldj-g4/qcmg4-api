package com.qcmg4.api.dto;


import com.qcmg4.api.models.Role;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
@Getter
@Setter
public class UserDto implements Serializable {
    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private String company;
    private Role role;
}
