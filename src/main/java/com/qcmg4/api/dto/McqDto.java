package com.qcmg4.api.dto;

import java.time.Instant;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class McqDto {
    private Long id;
    private String name;
    private String description;
    private String createdBy;
    private String lastEditionBy;
    private Instant createdAt;
    private Instant lastEditionAt;

    private Set<QuestionDto> questions;

}
