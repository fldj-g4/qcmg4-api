package com.qcmg4.api.dto;

import lombok.*;

@Data
public class QuestionDto {
    private Long id;
    private String question;

    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String answer5;
    private String good_answer;

    private Long idMcq;


    
}
