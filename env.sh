#!/bin/bash
echo "Start!"

# Ne pas mettre de commentaires ni de lignes vides dans 
# le fichier .env
# Ni autre chose que ce qui a la forme AZERTY_VALUE=myValue

filename=".env"
while read line; do
    echo "export $line"
    eval "export $line"
done < $filename